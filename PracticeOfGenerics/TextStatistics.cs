﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace PracticeOfGenerics
{
    public class TextStatistics
    {
        public static void ParsingText(string text)
        {
            Dictionary<string, int> statistics = new Dictionary<string, int>();
            string[] words = Regex.Replace(text, @"[.,]", string.Empty).Split(' ');
            int countWord = 0;
            foreach (var word in words)
            {
                if (statistics.ContainsKey(word))
                {
                    statistics[word]++;
                }
                else
                {
                    statistics.Add(word, 1);
                }
                countWord++;
            }
            int number = 0;
            Console.WriteLine("\tСлово: \t\tКол-во: ");
            foreach (var word in statistics)
            {
                Console.WriteLine($"{++number}: \t{word.Key}\t\t{word.Value}");
            }
            Console.WriteLine($"Всего слов: {countWord} из них уникальных: {statistics.Count}");
        }
    }
}
