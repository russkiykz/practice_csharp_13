﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace PracticeOfGenerics
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = "Вот дом, Который построил Джек. А это пшеница, Кото­рая в темном  чулане хранится В доме, Который построил Джек. А это веселая птица­ синица, Которая часто вору­ет пшеницу, Которая в темном чулане хранится В доме, Который построил Джек.";
            TextStatistics.ParsingText(text);
        }
    }
}
